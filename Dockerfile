FROM wordpress:cli-php7.2

USER root

ENV RUBY_MAJOUR_VER  2.4
ENV RUBY_VERSION     2.4.4


RUN apk add --no-cache \
		less \
        lftp \
        rsync \
        openssh-client \
        yaml-dev \
        mariadb-client

# SETUP: package install
RUN apk add --no-cache --virtual .ruby-builddeps \
    autoconf \
    bison \
    bzip2 \
    bzip2-dev \
    ca-certificates \
    coreutils \
    curl \
    gcc \
    g++ \
    make \
    gdbm-dev \
    glib-dev \
    libc-dev \
    libedit-dev \
    libffi-dev \
    libxml2-dev \
    libxslt-dev \
    libressl-dev \
    linux-headers \
    cyrus-sasl-dev \
    ncurses-dev \
    procps \
    tcl-dev \
    zlib-dev \
    tar \
    perl \
    python \
    py-pip \
    libmemcached-libs \
    mariadb mariadb-dev;

COPY ruby24.patch /tmp/ruby24.patch

RUN cd /tmp; \
    wget -q "http://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOUR_VER}/ruby-${RUBY_VERSION}.tar.gz" -O ruby-${RUBY_VERSION}.tar.gz && \
    tar zxf ruby-${RUBY_VERSION}.tar.gz && \
    cd ruby-${RUBY_VERSION} && \
    patch -u ./ext/openssl/extconf.rb < ../ruby24.patch && \
    ./configure --with-openssl-dir=/usr/local/libressl-2.7.4 --disable-install-doc --disable-install-rdoc && make && make install && \
    cd $HOME && \
    rm -rf /tmp/* && \
    echo "install: --no-document" > $HOME/.gemrc && \
    echo "update: --no-document" >> $HOME/.gemrc && \
    gem install wordmove && \
    apk del .ruby-builddeps --purge

USER www-data

