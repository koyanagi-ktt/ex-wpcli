## Extended WP-CLI Official Image

[![pipeline status](https://gitlab.com/koyanagi-ktt/ex-wpcli/badges/master/pipeline.svg)](https://gitlab.com/koyanagi-ktt/ex-wpcli/commits/master)

- wordmove
- lftp
- mariadb-client

```
docker pull registry.gitlab.com/koyanagi-ktt/ex-wpcli:latest
```
